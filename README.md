# Monitoring
This is a monitoring system for Russian Education Management.
Schools and Education Departments upload documents or links to document in this system.
Experts evaluate this link by criterias.

Expert evaluations downloaded for further work.

# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

# Prerequisites

You need to install ruby v 3.1 and rails version 6.1.4. 
Then clone repository and bundle install.

# Deployment

You can deploy this with cap deploy. Before deploying change file in config/deploy/production.rb

# Built With
Rails 6.1
Ruby 3.1

# Contributing

Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

# Versioning

We use SemVer for versioning. For the versions available, see the tags on this repository.

# Authors

Nikitin Sergey Vasilevich - Initial work
See also the list of contributors who participated in this project.

# License

This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments

Hat tip to anyone whose code was used