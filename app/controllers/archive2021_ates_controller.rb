class Archive2021AtesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @archive2021_ates = Archive2021Ate.accessible_by(current_ability).paginate(page: params[:page])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Mouo.accessible_by(current_ability).order("code")

    if !params[:search].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).search(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:ates].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).mouos(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil? and !params[:ates].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).filtr(params[:filter]).mouos(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:ates].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).search(params[:search]).mouos(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil? and !params[:ates].nil?
      @archive2021_ates = Archive2021Ate.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).mouos(params[:ates]).paginate(page: params[:page])
    end
    @archive2021_ates = @archive2021_ates.joins(:mouo).order('mouos.code ASC')
  end

  def export
    @archive2021_ates = Archive2021Ate.accessible_by(current_ability)
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    render xlsx: 'monitoring_objectivity_mouos_at_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/archive2021_ates/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @archive2021_ate = Archive2021Ate.new
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if current_user.mouo_role? and !current_user.admin?
      @archive2021_ates = Archive2021Ate.joins(:mouo).where(" mouos.code = #{current_user.target_id} ")
      if @archive2021_ates.count > 0
        @archive2021_ate = @archive2021_ates.first
        flash[:success] = t("archive2021_ates.errors.cant_create_new_privous_was")
        render :edit
      else
        @archive2021_ate = Archive2021Ate.new
        if !@archive2021_ate.nil?
          render :new
        else
          flash[:success] = t("archive2021_ates.errors.cant_create_new")
          render :index
        end
      end
    end
    if current_user.admin?
      if !@archive2021_ate.nil?
        render :edit
      else
        flash[:success] = t("archive2021_ates.errors.cant_create_new")
        redirect_to :action => 'index'
      end
    end
  end

  def show
    @archive2021_ate = Archive2021Ate.accessible_by(current_ability).find(params[:id])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
  end

  def create
    @archive2021_ate = Archive2021Ate.new(archive2021_ate_params)
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if @archive2021_ate.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @archive2021_ate = Archive2021Ate.accessible_by(current_ability).find(params[:id])
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
  end

  def delete
    # flash[:success] = t("archive2021_ates.notice.updated")
    Archive2021Ate.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @archive2021_ate = Archive2021Ate.accessible_by(current_ability).find(params[:id])
    @archive2021_ate.destroy
    respond_to do |format|
      format.html { redirect_to archive2021_ates_url, notice: t("archive2021_ates.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @archive2021_ate = Archive2021Ate.accessible_by(current_ability).find params[:id]
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if @archive2021_ate.update archive2021_ate_params
      respond_to do |format|
        format.html {
          flash[:success] = t("archive2021_ates.notice.updated")
          redirect_to :action => 'index'
       }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          render :action => 'edit'
        }
        format.json { respond_with_bip(@archive2021_ate) }
      end
    end

  end


  def archive2021_ate_params
    params.require(:archive2021_ate).permit(:mouo_id, :user_id, :status_id,
        :url01, :document01, :url02, :document02, :url03, :document03, :url04, :document04,:url05, :document05, :url06, :document06,
        :url07, :document07, :url08, :document08, :url09, :document09, :url10, :document10,:url11, :document11,
        :delete_document01, :delete_document02, :delete_document03, :delete_document04, :delete_document05, :delete_document06, :delete_document07,
        :delete_document08, :delete_document09, :delete_document10, :delete_document11)
  end
end
