class Archive2021OuMarksController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:filter].nil?
        @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:filter].nil?
        @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:search].nil? and !params[:filter].nil?
        @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability).filtr(params[:filter]).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def export
    @archive2021_ou_marks = Archive2021OuMark.accessible_by(current_ability)
    render xlsx: 'ous_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/archive2021_ou_marks/export.xlsx.axlsx'
  end

  def new
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).where (" id not in (select monitoring_objectivity_ou_id from archive2021_ou_marks) ")
    @archive2021_ou_mark_not_ended = Archive2021OuMark.accessible_by(current_ability).where(:user_id => current_user.id).where(:status => 0)
    if @archive2021_ou_mark_not_ended.count > 0
      @archive2021_ou_mark = @archive2021_ou_mark_not_ended.first
      @monitoring_objectivity_ou = MonitoringObjectivityOu.find(@archive2021_ou_mark.monitoring_objectivity_ou.id)
      flash[:success] = t("archive2021_ou_marks.errors.cant_create_new_privous_was")
      render :edit
    else
      if @monitoring_objectivity_ous.count > 0
        @monitoring_objectivity_ou = @monitoring_objectivity_ous[rand(@monitoring_objectivity_ous.count).to_i]
        @archive2021_ou_mark = Archive2021OuMark.new
        @archive2021_ou_mark.monitoring_objectivity_ou_id = @monitoring_objectivity_ou.id
        @archive2021_ou_mark.user_id = current_user.id
        @archive2021_ou_mark.mark01 = 0
        @archive2021_ou_mark.mark02 = 0
        @archive2021_ou_mark.mark03 = 0
        @archive2021_ou_mark.mark04 = 0
        @archive2021_ou_mark.mark05 = 0
        @archive2021_ou_mark.mark06 = 0
        @archive2021_ou_mark.mark07 = 0
        @archive2021_ou_mark.mark08 = 0
        @archive2021_ou_mark.mark09 = 0
        @archive2021_ou_mark.mark10 = 0
        @archive2021_ou_mark.mark11 = 0
        @archive2021_ou_mark.mark12 = 0
        @archive2021_ou_mark.mark13 = 0
        @archive2021_ou_mark.mark14 = 0
        @archive2021_ou_mark.mark15 = 0
        @archive2021_ou_mark.mark16 = 0
        @archive2021_ou_mark.status = 0
        if @monitoring_objectivity_ou.url01.present? or @monitoring_objectivity_ou.document01.present?
          @archive2021_ou_mark.mark01 = 1
        end
        if @monitoring_objectivity_ou.url02.present? or @monitoring_objectivity_ou.document02.present?
          @archive2021_ou_mark.mark02 = 1
        end
        if @monitoring_objectivity_ou.url03.present? or @monitoring_objectivity_ou.document03.present?
          @archive2021_ou_mark.mark03 = 1
        end
        if @monitoring_objectivity_ou.url04.present? or @monitoring_objectivity_ou.document04.present?
          @archive2021_ou_mark.mark04 = 1
        end
        if @monitoring_objectivity_ou.url05.present? or @monitoring_objectivity_ou.document05.present?
          @archive2021_ou_mark.mark05 = 1
        end
        if @monitoring_objectivity_ou.url06.present? or @monitoring_objectivity_ou.document06.present?
          @archive2021_ou_mark.mark06 = 1
        end
        if @monitoring_objectivity_ou.url07.present? or @monitoring_objectivity_ou.document07.present?
          @archive2021_ou_mark.mark07 = 1
        end
        if @monitoring_objectivity_ou.url08.present? or @monitoring_objectivity_ou.document08.present?
          @archive2021_ou_mark.mark08 = 1
        end
        if @monitoring_objectivity_ou.url09.present? or @monitoring_objectivity_ou.document09.present?
          @archive2021_ou_mark.mark09 = 1
        end
        if @monitoring_objectivity_ou.url10.present? or @monitoring_objectivity_ou.document10.present?
          @archive2021_ou_mark.mark10 = 1
        end
        if @monitoring_objectivity_ou.url11.present? or @monitoring_objectivity_ou.document11.present?
          @archive2021_ou_mark.mark11 = 1
        end
        if @monitoring_objectivity_ou.url12.present? or @monitoring_objectivity_ou.document12.present?
          @archive2021_ou_mark.mark12 = 1
        end
        if @monitoring_objectivity_ou.url13.present? or @monitoring_objectivity_ou.document13.present?
          @archive2021_ou_mark.mark13 = 1
        end
        if @monitoring_objectivity_ou.url14.present? or @monitoring_objectivity_ou.document14.present?
          @archive2021_ou_mark.mark14 = 1
        end
        if @monitoring_objectivity_ou.url15.present? or @monitoring_objectivity_ou.document15.present?
          @archive2021_ou_mark.mark15 = 1
        end
        if @monitoring_objectivity_ou.url16.present? or @monitoring_objectivity_ou.document16.present?
          @archive2021_ou_mark.mark16 = 1
        end
        sleep rand(2)
        @archive2021_ou_mark2 = Archive2021OuMark.where(:monitoring_objectivity_ou_id => @monitoring_objectivity_ou.id)
        if !@archive2021_ou_mark.nil? and @archive2021_ou_mark2.count == 0
          @archive2021_ou_mark.monitoring_objectivity_ou = @monitoring_objectivity_ou
          @archive2021_ou_mark.save
          render :edit
        else
          flash[:success] = t("archive2021_ou_marks.errors.cant_create_new")
          redirect_to :action => 'index'
        end
      else
        flash[:success] = t("archive2021_ou_marks.errors.no_new")
        redirect_to :action => 'index'
      end
    end
  end

  def show
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @archive2021_ou_mark = Archive2021OuMark.accessible_by(current_ability).find(params[:id])
  end

  def create
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Ou.accessible_by(current_ability)
    @mouos = Mouo.accessible_by(current_ability)
    @archive2021_ou_mark = Archive2021OuMark.new(archive2021_ou_mark_params)
    if @archive2021_ou_mark.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ates = Ou.all
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @archive2021_ou_mark = Archive2021OuMark.find(params[:id])
    @monitoring_objectivity_ou = MonitoringObjectivityOu.find(@archive2021_ou_mark.monitoring_objectivity_ou.id)
  end

  def delete
    Archive2021OuMark.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @archive2021_ou_mark = Archive2021OuMark.accessible_by(current_ability).find(params[:id])
    @archive2021_ou_mark.destroy
    respond_to do |format|
      format.html { redirect_to ous_url, notice: t("archive2021_ou_marks.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @archive2021_ou_mark = Archive2021OuMark.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @archive2021_ou_mark.update archive2021_ou_mark_params
      flash[:success] = t("archive2021_ou_marks.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end


  def archive2021_ou_mark_params
    params.require(:archive2021_ou_mark).permit(:id, :monitoring_objectivity_ou_id, :user_id, :status, :mark01, :mark02, :mark03, :mark04, :mark05, :mark06, :mark07, :mark08, :mark09, :mark10, :mark11, :mark12, :mark13, :mark14, :mark15, :mark16, :notes)
  end

end
