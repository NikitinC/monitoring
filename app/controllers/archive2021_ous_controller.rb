class Archive2021OusController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @archive2021_ous = Archive2021Ou.accessible_by(current_ability).paginate(page: params[:page])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Ate.accessible_by(current_ability)

    if !params[:search].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).search(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:ates].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).ates(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil? and !params[:ates].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).filtr(params[:filter]).ates(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:ates].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).search(params[:search]).ates(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil? and !params[:ates].nil?
      @archive2021_ous = Archive2021Ou.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).ates(params[:ates]).paginate(page: params[:page])
    end
    @archive2021_ous = @archive2021_ous.joins(:ou).order('ous.code ASC')
  end

  def export
    @archive2021_ous = Archive2021Ou.accessible_by(current_ability)
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    render xlsx: 'archive2021_ous_at_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/archive2021_ous/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if current_user.ous_role?
      @archive2021_ous = Archive2021Ou.joins(:ou).where(" ous.code = #{current_user.target_id} ")
      if @archive2021_ous.count > 0
        @archive2021_ou = @archive2021_ous.first
        flash[:success] = t("archive2021_ous.errors.cant_create_new_privous_was")
        render :edit
      else
        @archive2021_ou = Archive2021Ou.new
        if !@archive2021_ou.nil?
          render :new
        else
          flash[:success] = t("archive2021_ous.errors.cant_create_new")
          render :index
        end
      end
    end
    if current_user.mouo_role? or current_user.admin?
      @archive2021_ou = Archive2021Ou.new
    end
    # @archive2021_ou = Archive2021Ou.new
    # if !@archive2021_ou.nil?
    #   render :new
    # else
    #   flash[:success] = t("archive2021_ous.errors.cant_create_new")
    #   render :index
    # end
  end

  def show
    @archive2021_ou = Archive2021Ou.accessible_by(current_ability).find(params[:id])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
  end

  def create
    @archive2021_ou = Archive2021Ou.new(archive2021_ou_params)
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if @archive2021_ou.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @archive2021_ou = Archive2021Ou.accessible_by(current_ability).find(params[:id])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    respond_to do |format|
      format.html { render :edit }
      format.json { respond_with_bip(@archive2021_ou) }
    end
  end

  def delete
    # flash[:success] = t("archive2021_ous.notice.updated")
    Archive2021Ou.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @archive2021_ou = Archive2021Ou.accessible_by(current_ability).find(params[:id])
    @archive2021_ou.destroy
    respond_to do |format|
      format.html { redirect_to archive2021_ous_url, notice: t("archive2021_ous.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  respond_to :html, :json

  def update
    @archive2021_ou = Archive2021Ou.accessible_by(current_ability).find params[:id]
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    # respond_to do |format|
    #   if @archive2021_ou.update archive2021_ou_params
    #     format.html { redirect_to(@archive2021_ou, t("archive2021_ous.notice.updated")) }
    #     format.json { respond_with_bip(@archive2021_ou) }
    #   else
    #     format.html { render :action => "edit" }
    #     format.json { respond_with_bip(@archive2021_ou) }
    #   end
    # end
    if @archive2021_ou.update archive2021_ou_params
      respond_to do |format|
        format.html {
          flash[:success] = t("archive2021_ous.notice.updated")
          redirect_to :action => 'index'
       }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          render :action => 'edit'
        }
        format.json { respond_with_bip(@archive2021_ou) }
      end
    end

  end


  def archive2021_ou_params
    params.require(:archive2021_ou).permit(:ou_id, :user_id, :status_id,
        :url01, :document01, :url02, :document02, :url03, :document03, :url04, :document04,:url05, :document05, :url06, :document06,
        :url07, :document07, :url08, :document08, :url09, :document09, :url10, :document10,:url11, :document11, :url12, :document12,
        :url13, :document13, :url14, :document14, :url15, :document15, :url16, :document16,
        :delete_document01, :delete_document02, :delete_document03, :delete_document04, :delete_document05, :delete_document06, :delete_document07,
        :delete_document08, :delete_document09, :delete_document10, :delete_document11, :delete_document12, :delete_document13, :delete_document14,
        :delete_document15, :delete_document16)
  end
end
