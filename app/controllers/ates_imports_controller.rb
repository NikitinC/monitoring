class AtesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @ates_import = AtesImport.new
  end

  def create
    @ates_import = AtesImport.new(params[:ates_import])
    render :create
  end

end
