class MonitoringObjectivityAtesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).paginate(page: params[:page])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Mouo.accessible_by(current_ability).order("code")

    if !params[:search].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).search(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:ates].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).mouos(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil? and !params[:ates].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).filtr(params[:filter]).mouos(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:ates].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).search(params[:search]).mouos(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil? and !params[:ates].nil?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).mouos(params[:ates]).paginate(page: params[:page])
    end
    @monitoring_objectivity_ates = @monitoring_objectivity_ates.joins(:mouo).order('mouos.code ASC')
  end

  def export
    @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability)
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    render xlsx: 'monitoring_objectivity_mouos_at_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/monitoring_objectivity_ates/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @monitoring_objectivity_ate = MonitoringObjectivityAte.new
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if current_user.mouo_role? and !current_user.admin?
      @monitoring_objectivity_ates = MonitoringObjectivityAte.joins(:mouo).where(" mouos.code = #{current_user.target_id} ")
      if @monitoring_objectivity_ates.count > 0
        @monitoring_objectivity_ate = @monitoring_objectivity_ates.first
        flash[:success] = t("monitoring_objectivity_ates.errors.cant_create_new_privous_was")
        render :edit
      else
        @monitoring_objectivity_ate = MonitoringObjectivityAte.new
        if !@monitoring_objectivity_ate.nil?
          render :new
        else
          flash[:success] = t("monitoring_objectivity_ates.errors.cant_create_new")
          render :index
        end
      end
    end
    if current_user.admin?
      if !@monitoring_objectivity_ate.nil?
        render :edit
      else
        flash[:success] = t("monitoring_objectivity_ates.errors.cant_create_new")
        redirect_to :action => 'index'
      end
    end
  end

  def show
    @monitoring_objectivity_ate = MonitoringObjectivityAte.accessible_by(current_ability).find(params[:id])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
  end

  def create
    @monitoring_objectivity_ate = MonitoringObjectivityAte.new(monitoring_objectivity_ate_params)
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if @monitoring_objectivity_ate.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @monitoring_objectivity_ate = MonitoringObjectivityAte.accessible_by(current_ability).find(params[:id])
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
  end

  def delete
    # flash[:success] = t("monitoring_objectivity_ates.notice.updated")
    MonitoringObjectivityAte.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @monitoring_objectivity_ate = MonitoringObjectivityAte.accessible_by(current_ability).find(params[:id])
    @monitoring_objectivity_ate.destroy
    respond_to do |format|
      format.html { redirect_to monitoring_objectivity_ates_url, notice: t("monitoring_objectivity_ates.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @monitoring_objectivity_ate = MonitoringObjectivityAte.accessible_by(current_ability).find params[:id]
    @ates = Mouo.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]

    if params["delete_document01"]=='1'
      @monitoring_objectivity_ate.document01.purge
    end
    if params["delete_document02"]=='1'
      @monitoring_objectivity_ate.document02.purge
    end
    if params["delete_document03"]=='1'
      @monitoring_objectivity_ate.document03.purge
    end
    if params["delete_document04"]=='1'
      @monitoring_objectivity_ate.document04.purge
    end
    if params["delete_document05"]=='1'
      @monitoring_objectivity_ate.document05.purge
    end
    if params["delete_document06"]=='1'
      @monitoring_objectivity_ate.document06.purge
    end
    if params["delete_document07"]=='1'
      @monitoring_objectivity_ate.document07.purge
    end
    if params["delete_document08"]=='1'
      @monitoring_objectivity_ate.document08.purge
    end
    if params["delete_document09"]=='1'
      @monitoring_objectivity_ate.document09.purge
    end
    if params["delete_document10"]=='1'
      @monitoring_objectivity_ate.document10.purge
    end

    if params["delete_document11"]=='1'
      @monitoring_objectivity_ate.document11.purge
    end
    if params["delete_document12"]=='1'
      @monitoring_objectivity_ate.document12.purge
    end
    if params["delete_document13"]=='1'
      @monitoring_objectivity_ate.document13.purge
    end
    if params["delete_document14"]=='1'
      @monitoring_objectivity_ate.document14.purge
    end
    if params["delete_document15"]=='1'
      @monitoring_objectivity_ate.document15.purge
    end
    if params["delete_document16"]=='1'
      @monitoring_objectivity_ate.document16.purge
    end
    if params["delete_document17"]=='1'
      @monitoring_objectivity_ate.document17.purge
    end


    if @monitoring_objectivity_ate.update monitoring_objectivity_ate_params
      respond_to do |format|
        format.html {
          flash[:success] = t("monitoring_objectivity_ates.notice.updated")
          redirect_to :action => 'index'
       }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          render :action => 'edit'
        }
        format.json { respond_with_bip(@monitoring_objectivity_ate) }
      end
    end

  end


  def monitoring_objectivity_ate_params
    params.require(:monitoring_objectivity_ate).permit(:mouo_id, :user_id, :status_id,
        :url01, :document01, :url02, :document02, :url03, :document03, :url04, :document04,:url05, :document05, :url06, :document06,
        :url07, :document07, :url08, :document08, :url09, :document09, :url10, :document10,:url11, :document11, :document12, :document13, :document14,
        :document15, :document16, :document17, :url12, :url13, :url14, :url15, :url16, :url17,
        :delete_document01, :delete_document02, :delete_document03, :delete_document04, :delete_document05, :delete_document06, :delete_document07,
        :delete_document08, :delete_document09, :delete_document10, :delete_document11, :delete_document12, :delete_document13, :delete_document14,
        :delete_document15, :delete_document16, :delete_document17, :notes)
  end
end
