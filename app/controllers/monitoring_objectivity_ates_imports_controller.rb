class MonitoringObjectivityAtesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @monitoring_objectivity_ates_import = MonitoringObjectivityAtesImport.new
  end

  def create
    @monitoring_objectivity_ates_import = MonitoringObjectivityAtesImport.new(params[:monitoring_objectivity_ates_import])
    render :create
  end

end
