class MonitoringObjectivityOusController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).paginate(page: params[:page])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Ate.accessible_by(current_ability)

    if !params[:search].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).search(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:ates].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).ates(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).paginate(page: params[:page])
    end
    if !params[:filter].nil? and !params[:ates].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).filtr(params[:filter]).ates(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:ates].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).search(params[:search]).ates(params[:ates]).paginate(page: params[:page])
    end
    if !params[:search].nil? and !params[:filter].nil? and !params[:ates].nil?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).search(params[:search]).filtr(params[:filter]).ates(params[:ates]).paginate(page: params[:page])
    end
    @monitoring_objectivity_ous = @monitoring_objectivity_ous.joins(:ou).order('ous.code ASC')
  end

  def export
    @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability)
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    render xlsx: 'monitoring_objectivity_ous_at_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/monitoring_objectivity_ous/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if current_user.ous_role?
      @monitoring_objectivity_ous = MonitoringObjectivityOu.joins(:ou).where(" ous.code = #{current_user.target_id} ")
      if @monitoring_objectivity_ous.count > 0
        @monitoring_objectivity_ou = @monitoring_objectivity_ous.first
        flash[:success] = t("monitoring_objectivity_ous.errors.cant_create_new_privous_was")
        render :edit
      else
        @monitoring_objectivity_ou = MonitoringObjectivityOu.new
        if !@monitoring_objectivity_ou.nil?
          render :new
        else
          flash[:success] = t("monitoring_objectivity_ous.errors.cant_create_new")
          render :index
        end
      end
    end
    if current_user.mouo_role? or current_user.admin?
      @monitoring_objectivity_ou = MonitoringObjectivityOu.new
    end
    # @monitoring_objectivity_ou = MonitoringObjectivityOu.new
    # if !@monitoring_objectivity_ou.nil?
    #   render :new
    # else
    #   flash[:success] = t("monitoring_objectivity_ous.errors.cant_create_new")
    #   render :index
    # end
  end

  def show
    @monitoring_objectivity_ou = MonitoringObjectivityOu.accessible_by(current_ability).find(params[:id])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
  end

  def create
    @monitoring_objectivity_ou = MonitoringObjectivityOu.new(monitoring_objectivity_ou_params)
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    if @monitoring_objectivity_ou.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @monitoring_objectivity_ou = MonitoringObjectivityOu.accessible_by(current_ability).find(params[:id])
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    respond_to do |format|
      format.html { render :edit }
      format.json { respond_with_bip(@monitoring_objectivity_ou) }
    end
  end

  def delete
    # flash[:success] = t("monitoring_objectivity_ous.notice.updated")
    MonitoringObjectivityOu.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @monitoring_objectivity_ou = MonitoringObjectivityOu.accessible_by(current_ability).find(params[:id])
    @monitoring_objectivity_ou.destroy
    respond_to do |format|
      format.html { redirect_to monitoring_objectivity_ous_url, notice: t("monitoring_objectivity_ous.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  respond_to :html, :json

  def update
    @monitoring_objectivity_ou = MonitoringObjectivityOu.accessible_by(current_ability).find params[:id]
    @ous = Ou.accessible_by(current_ability).order("code")
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    # respond_to do |format|
    #   if @monitoring_objectivity_ou.update monitoring_objectivity_ou_params
    #     format.html { redirect_to(@monitoring_objectivity_ou, t("monitoring_objectivity_ous.notice.updated")) }
    #     format.json { respond_with_bip(@monitoring_objectivity_ou) }
    #   else
    #     format.html { render :action => "edit" }
    #     format.json { respond_with_bip(@monitoring_objectivity_ou) }
    #   end
    # end

    # before_save do
    #   if self.delete_document01='1' && self.document01.present?
    #     self.document01.purge
    #     self.delete_document01 = nil
    #   end
    # end

    if params["delete_document01"]=='1'
      @monitoring_objectivity_ou.document01.purge
    end
    if params["delete_document02"]=='1'
      @monitoring_objectivity_ou.document02.purge
    end
    if params["delete_document03"]=='1'
      @monitoring_objectivity_ou.document03.purge
    end
    if params["delete_document04"]=='1'
      @monitoring_objectivity_ou.document04.purge
    end
    if params["delete_document05"]=='1'
      @monitoring_objectivity_ou.document05.purge
    end
    if params["delete_document06"]=='1'
      @monitoring_objectivity_ou.document06.purge
    end
    if params["delete_document07"]=='1'
      @monitoring_objectivity_ou.document07.purge
    end
    if params["delete_document08"]=='1'
      @monitoring_objectivity_ou.document08.purge
    end
    if params["delete_document09"]=='1'
      @monitoring_objectivity_ou.document09.purge
    end
    if params["delete_document10"]=='1'
      @monitoring_objectivity_ou.document10.purge
    end

    if params["delete_document11"]=='1'
      @monitoring_objectivity_ou.document11.purge
    end
    if params["delete_document12"]=='1'
      @monitoring_objectivity_ou.document12.purge
    end
    if params["delete_document13"]=='1'
      @monitoring_objectivity_ou.document13.purge
    end
    if params["delete_document14"]=='1'
      @monitoring_objectivity_ou.document14.purge
    end
    if params["delete_document15"]=='1'
      @monitoring_objectivity_ou.document15.purge
    end
    if params["delete_document16"]=='1'
      @monitoring_objectivity_ou.document16.purge
    end
    if params["delete_document17"]=='1'
      @monitoring_objectivity_ou.document17.purge
    end
    if params["delete_document18"]=='1'
      @monitoring_objectivity_ou.document18.purge
    end
    if params["delete_document19"]=='1'
      @monitoring_objectivity_ou.document19.purge
    end
    if params["delete_document20"]=='1'
      @monitoring_objectivity_ou.document20.purge
    end
    if params["delete_document21"]=='1'
      @monitoring_objectivity_ou.document21.purge
    end
    if params["delete_document22"]=='1'
      @monitoring_objectivity_ou.document22.purge
    end

    if @monitoring_objectivity_ou.update monitoring_objectivity_ou_params
      respond_to do |format|
        format.html {
          flash[:success] = t("monitoring_objectivity_ous.notice.updated")
          redirect_to :action => 'index'
       }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          render :action => 'edit'
        }
        format.json { respond_with_bip(@monitoring_objectivity_ou) }
      end
    end

  end


  def monitoring_objectivity_ou_params
    params.require(:monitoring_objectivity_ou).permit(:ou_id, :user_id, :status_id,
        :url01, :document01, :url02, :document02, :url03, :document03, :url04, :document04, :url05, :document05, :url06, :document06,
        :url07, :document07, :url08, :document08, :url09, :document09, :url10, :document10, :url11, :document11, :url12, :document12,
        :url13, :document13, :url14, :document14, :url15, :document15, :url16, :document16, :url17, :document17, :url18, :document18,
        :url19, :document19, :url20, :document20, :url21, :document21, :url22, :document22,
        :delete_document01, :delete_document02, :delete_document03, :delete_document04, :delete_document05, :delete_document06, :delete_document07,
        :delete_document08, :delete_document09, :delete_document10, :delete_document11, :delete_document12, :delete_document13, :delete_document14,
        :delete_document15, :delete_document16, :delete_document17, :delete_document18, :delete_document19, :delete_document20, :delete_document21,
        :delete_document22, :notes)
  end
end
