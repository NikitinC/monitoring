class MonitoringObjectivityOusImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @monitoring_objectivity_ous_import = MonitoringObjectivityOusImport.new
  end

  def create
    @monitoring_objectivity_ous_import = MonitoringObjectivityOusImport.new(params[:monitoring_objectivity_ous_import])
    render :create
  end

end
