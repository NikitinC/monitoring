class ObjectivityAteMarksController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:filter].nil?
        @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:filter].nil?
        @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:search].nil? and !params[:filter].nil?
        @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability).filtr(params[:filter]).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def export
    @objectivity_ate_marks = ObjectivityAteMark.accessible_by(current_ability)
    render xlsx: 'ous_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/objectivity_ate_marks/export.xlsx.axlsx'
  end

  def new
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @monitoring_objectivity_ates = MonitoringObjectivityAte.accessible_by(current_ability).where (" id not in (select monitoring_objectivity_ate_id from objectivity_ate_marks) ")
    @objectivity_ate_mark_not_ended = ObjectivityAteMark.accessible_by(current_ability).where(:user_id => current_user.id).where(:status => 0)
    if @objectivity_ate_mark_not_ended.count > 0
      @objectivity_ate_mark = @objectivity_ate_mark_not_ended.first
      @monitoring_objectivity_ate = MonitoringObjectivityAte.find(@objectivity_ate_mark.monitoring_objectivity_ate.id)
      flash[:success] = t("objectivity_ate_marks.errors.cant_create_new_privous_was")
      render :edit
    else
      if @monitoring_objectivity_ates.count > 0
        @monitoring_objectivity_ate = @monitoring_objectivity_ates[rand(@monitoring_objectivity_ates.count).to_i]
        @objectivity_ate_mark = ObjectivityAteMark.new
        @objectivity_ate_mark.monitoring_objectivity_ate_id = @monitoring_objectivity_ate.id
        @objectivity_ate_mark.user_id = current_user.id
        @objectivity_ate_mark.mark01 = 0
        @objectivity_ate_mark.mark02 = 0
        @objectivity_ate_mark.mark03 = 0
        @objectivity_ate_mark.mark04 = 0
        @objectivity_ate_mark.mark05 = 0
        @objectivity_ate_mark.mark06 = 0
        @objectivity_ate_mark.mark07 = 0
        @objectivity_ate_mark.mark08 = 0
        @objectivity_ate_mark.mark09 = 0
        @objectivity_ate_mark.mark10 = 0
        @objectivity_ate_mark.mark11 = 0
        @objectivity_ate_mark.mark12 = 0
        @objectivity_ate_mark.mark13 = 0
        @objectivity_ate_mark.mark14 = 0
        @objectivity_ate_mark.mark15 = 0
        @objectivity_ate_mark.mark16 = 0
        @objectivity_ate_mark.mark17 = 0
        @objectivity_ate_mark.mark18 = 0
        @objectivity_ate_mark.mark19 = 0
        @objectivity_ate_mark.mark20 = 0
        @objectivity_ate_mark.mark21 = 0
        @objectivity_ate_mark.mark22 = 0
        @objectivity_ate_mark.mark23 = 0
        @objectivity_ate_mark.mark24 = 0
        @objectivity_ate_mark.mark25 = 0
        @objectivity_ate_mark.mark26 = 0
        @objectivity_ate_mark.mark27 = 0
        @objectivity_ate_mark.mark28 = 0
        @objectivity_ate_mark.mark29 = 0
        @objectivity_ate_mark.mark30 = 0
        @objectivity_ate_mark.mark31 = 0
        @objectivity_ate_mark.mark32 = 0
        @objectivity_ate_mark.mark33 = 0
        @objectivity_ate_mark.mark34 = 0
        @objectivity_ate_mark.mark35 = 0
        @objectivity_ate_mark.mark36 = 0
        @objectivity_ate_mark.mark37 = 0
        @objectivity_ate_mark.mark38 = 0
        @objectivity_ate_mark.mark39 = 0
        @objectivity_ate_mark.mark40 = 0
        @objectivity_ate_mark.mark41 = 0
        @objectivity_ate_mark.status = 0
        if @monitoring_objectivity_ate.url01.present? or @monitoring_objectivity_ate.document01.present?
          @objectivity_ate_mark.mark01 = 1
        end
        if @monitoring_objectivity_ate.url02.present? or @monitoring_objectivity_ate.document02.present?
          @objectivity_ate_mark.mark02 = 1
        end
        if @monitoring_objectivity_ate.url03.present? or @monitoring_objectivity_ate.document03.present?
          @objectivity_ate_mark.mark03 = 1
        end
        if @monitoring_objectivity_ate.url04.present? or @monitoring_objectivity_ate.document04.present?
          @objectivity_ate_mark.mark04 = 1
        end
        if @monitoring_objectivity_ate.url05.present? or @monitoring_objectivity_ate.document05.present?
          @objectivity_ate_mark.mark05 = 1
        end
        if @monitoring_objectivity_ate.url06.present? or @monitoring_objectivity_ate.document06.present?
          @objectivity_ate_mark.mark06 = 1
        end
        if @monitoring_objectivity_ate.url07.present? or @monitoring_objectivity_ate.document07.present?
          @objectivity_ate_mark.mark07 = 1
        end
        if @monitoring_objectivity_ate.url08.present? or @monitoring_objectivity_ate.document08.present?
          @objectivity_ate_mark.mark08 = 1
        end
        if @monitoring_objectivity_ate.url09.present? or @monitoring_objectivity_ate.document09.present?
          @objectivity_ate_mark.mark09 = 1
        end
        if @monitoring_objectivity_ate.url10.present? or @monitoring_objectivity_ate.document10.present?
          @objectivity_ate_mark.mark10 = 1
        end
        if @monitoring_objectivity_ate.url11.present? or @monitoring_objectivity_ate.document11.present?
          @objectivity_ate_mark.mark11 = 1
        end
        sleep rand(2)
        @objectivity_ate_mark2 = ObjectivityAteMark.where(:monitoring_objectivity_ate_id => @monitoring_objectivity_ate.id)
        if !@objectivity_ate_mark.nil? and @objectivity_ate_mark2.count == 0
          @objectivity_ate_mark.monitoring_objectivity_ate = @monitoring_objectivity_ate
          @objectivity_ate_mark.save
          render :edit
        else
          flash[:success] = t("objectivity_ate_marks.errors.cant_create_new")
          redirect_to :action => 'index'
        end
      else
        flash[:success] = t("objectivity_ate_marks.errors.no_new")
        redirect_to :action => 'index'
      end
    end
  end

  def show
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @objectivity_ate_mark = ObjectivityAteMark.accessible_by(current_ability).find(params[:id])
  end

  def create
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Ate.accessible_by(current_ability)
    @mouos = Mouo.accessible_by(current_ability)
    @objectivity_ate_mark = ObjectivityAteMark.new(objectivity_ate_mark_params)
    if @objectivity_ate_mark.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ates = Ate.all
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @objectivity_ate_mark = ObjectivityAteMark.find(params[:id])
    @monitoring_objectivity_ate = MonitoringObjectivityAte.find(@objectivity_ate_mark.monitoring_objectivity_ate.id)
  end

  def delete
    ObjectivityAteMark.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @objectivity_ate_mark = ObjectivityAteMark.accessible_by(current_ability).find(params[:id])
    @objectivity_ate_mark.destroy
    respond_to do |format|
      format.html { redirect_to ous_url, notice: t("objectivity_ate_marks.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @objectivity_ate_mark = ObjectivityAteMark.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @objectivity_ate_mark.update objectivity_ate_mark_params
      flash[:success] = t("objectivity_ate_marks.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end


  def objectivity_ate_mark_params
    params.require(:objectivity_ate_mark).permit(:id, :monitoring_objectivity_ate_id, :user_id, :status,
        :mark01, :mark02, :mark03, :mark04, :mark05, :mark06, :mark07, :mark08, :mark09, :mark10,
        :mark11, :mark12, :mark13, :mark14, :mark15, :mark16, :mark17, :mark18, :mark19, :mark20,
        :mark21, :mark22, :mark23, :mark24, :mark25, :mark26, :mark27, :mark28, :mark29, :mark30,
        :mark31, :mark32, :mark33, :mark34, :mark35, :mark36, :mark37, :mark38, :mark39, :mark40,
        :mark41, :notes)
  end

end
