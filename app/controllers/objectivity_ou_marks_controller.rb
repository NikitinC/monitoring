class ObjectivityOuMarksController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:filter].nil?
        @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:filter].nil?
        @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability).filtr(params[:filter]).paginate(page: params[:page]).order('id ASC')
      end
      if !params[:search].nil? and !params[:filter].nil?
        @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability).filtr(params[:filter]).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def export
    @objectivity_ou_marks = ObjectivityOuMark.accessible_by(current_ability)
    render xlsx: 'ous_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/objectivity_ou_marks/export.xlsx.axlsx'
  end

  def new
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @monitoring_objectivity_ous = MonitoringObjectivityOu.accessible_by(current_ability).where (" id not in (select monitoring_objectivity_ou_id from objectivity_ou_marks) ")
    @objectivity_ou_mark_not_ended = ObjectivityOuMark.accessible_by(current_ability).where(:user_id => current_user.id).where(:status => 0)
    if @objectivity_ou_mark_not_ended.count > 0
      @objectivity_ou_mark = @objectivity_ou_mark_not_ended.first
      @monitoring_objectivity_ou = MonitoringObjectivityOu.find(@objectivity_ou_mark.monitoring_objectivity_ou.id)
      flash[:success] = t("objectivity_ou_marks.errors.cant_create_new_privous_was")
      render :edit
    else
      if @monitoring_objectivity_ous.count > 0
        @monitoring_objectivity_ou = @monitoring_objectivity_ous[rand(@monitoring_objectivity_ous.count).to_i]
        @objectivity_ou_mark = ObjectivityOuMark.new
        @objectivity_ou_mark.monitoring_objectivity_ou_id = @monitoring_objectivity_ou.id
        @objectivity_ou_mark.user_id = current_user.id
        @objectivity_ou_mark.mark01 = 0
        @objectivity_ou_mark.mark02 = 0
        @objectivity_ou_mark.mark03 = 0
        @objectivity_ou_mark.mark04 = 0
        @objectivity_ou_mark.mark05 = 0
        @objectivity_ou_mark.mark06 = 0
        @objectivity_ou_mark.mark07 = 0
        @objectivity_ou_mark.mark08 = 0
        @objectivity_ou_mark.mark09 = 0
        @objectivity_ou_mark.mark10 = 0
        @objectivity_ou_mark.mark11 = 0
        @objectivity_ou_mark.mark12 = 0
        @objectivity_ou_mark.mark13 = 0
        @objectivity_ou_mark.mark14 = 0
        @objectivity_ou_mark.mark15 = 0
        @objectivity_ou_mark.mark16 = 0
        @objectivity_ou_mark.mark17 = 0
        @objectivity_ou_mark.mark18 = 0
        @objectivity_ou_mark.mark19 = 0
        @objectivity_ou_mark.mark20 = 0
        @objectivity_ou_mark.mark21 = 0
        @objectivity_ou_mark.mark22 = 0
        @objectivity_ou_mark.mark23 = 0
        @objectivity_ou_mark.mark24 = 0
        @objectivity_ou_mark.status = 0
        if @monitoring_objectivity_ou.url01.present? or @monitoring_objectivity_ou.document01.present?
          @objectivity_ou_mark.mark01 = 1
        end
        if @monitoring_objectivity_ou.url02.present? or @monitoring_objectivity_ou.document02.present?
          @objectivity_ou_mark.mark02 = 1
        end
        if @monitoring_objectivity_ou.url03.present? or @monitoring_objectivity_ou.document03.present?
          @objectivity_ou_mark.mark03 = 1
        end
        if @monitoring_objectivity_ou.url04.present? or @monitoring_objectivity_ou.document04.present?
          @objectivity_ou_mark.mark04 = 1
        end
        if @monitoring_objectivity_ou.url05.present? or @monitoring_objectivity_ou.document05.present?
          @objectivity_ou_mark.mark05 = 1
        end
        if @monitoring_objectivity_ou.url06.present? or @monitoring_objectivity_ou.document06.present?
          @objectivity_ou_mark.mark06 = 1
        end
        if @monitoring_objectivity_ou.url07.present? or @monitoring_objectivity_ou.document07.present?
          @objectivity_ou_mark.mark07 = 1
        end
        if @monitoring_objectivity_ou.url08.present? or @monitoring_objectivity_ou.document08.present?
          @objectivity_ou_mark.mark08 = 1
        end
        if @monitoring_objectivity_ou.url09.present? or @monitoring_objectivity_ou.document09.present?
          @objectivity_ou_mark.mark09 = 1
        end
        if @monitoring_objectivity_ou.url10.present? or @monitoring_objectivity_ou.document10.present?
          @objectivity_ou_mark.mark10 = 1
        end
        if @monitoring_objectivity_ou.url11.present? or @monitoring_objectivity_ou.document11.present?
          @objectivity_ou_mark.mark11 = 1
        end
        if @monitoring_objectivity_ou.url12.present? or @monitoring_objectivity_ou.document12.present?
          @objectivity_ou_mark.mark12 = 1
        end
        if @monitoring_objectivity_ou.url13.present? or @monitoring_objectivity_ou.document13.present?
          @objectivity_ou_mark.mark13 = 1
        end
        if @monitoring_objectivity_ou.url14.present? or @monitoring_objectivity_ou.document14.present?
          @objectivity_ou_mark.mark14 = 1
        end
        if @monitoring_objectivity_ou.url15.present? or @monitoring_objectivity_ou.document15.present?
          @objectivity_ou_mark.mark15 = 1
        end
        if @monitoring_objectivity_ou.url16.present? or @monitoring_objectivity_ou.document16.present?
          @objectivity_ou_mark.mark16 = 1
        end
        if @monitoring_objectivity_ou.url17.present? or @monitoring_objectivity_ou.document17.present?
          @objectivity_ou_mark.mark17 = 1
        end
        if @monitoring_objectivity_ou.url18.present? or @monitoring_objectivity_ou.document18.present?
          @objectivity_ou_mark.mark18 = 1
        end
        if @monitoring_objectivity_ou.url19.present? or @monitoring_objectivity_ou.document19.present?
          @objectivity_ou_mark.mark19 = 1
        end
        if @monitoring_objectivity_ou.url20.present? or @monitoring_objectivity_ou.document20.present?
          @objectivity_ou_mark.mark20 = 1
        end
        if @monitoring_objectivity_ou.url21.present? or @monitoring_objectivity_ou.document21.present?
          @objectivity_ou_mark.mark21 = 1
        end
        if @monitoring_objectivity_ou.url22.present? or @monitoring_objectivity_ou.document22.present?
          @objectivity_ou_mark.mark22 = 1
        end
        sleep rand(2)
        @objectivity_ou_mark2 = ObjectivityOuMark.where(:monitoring_objectivity_ou_id => @monitoring_objectivity_ou.id)
        if !@objectivity_ou_mark.nil? and @objectivity_ou_mark2.count == 0
          @objectivity_ou_mark.monitoring_objectivity_ou = @monitoring_objectivity_ou
          @objectivity_ou_mark.save
          render :edit
        else
          flash[:success] = t("objectivity_ou_marks.errors.cant_create_new")
          redirect_to :action => 'index'
        end
      else
        flash[:success] = t("objectivity_ou_marks.errors.no_new")
        redirect_to :action => 'index'
      end
    end
  end

  def show
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @objectivity_ou_mark = ObjectivityOuMark.accessible_by(current_ability).find(params[:id])
  end

  def create
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @ates = Ou.accessible_by(current_ability)
    @mouos = Mouo.accessible_by(current_ability)
    @objectivity_ou_mark = ObjectivityOuMark.new(objectivity_ou_mark_params)
    if @objectivity_ou_mark.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ates = Ou.all
    @statuses = [{"name"=>"В работе", "id"=> 0}, {"name"=>"Завершена", "id"=>1}]
    @objectivity_ou_mark = ObjectivityOuMark.find(params[:id])
    @monitoring_objectivity_ou = MonitoringObjectivityOu.find(@objectivity_ou_mark.monitoring_objectivity_ou.id)
  end

  def delete
    ObjectivityOuMark.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @objectivity_ou_mark = ObjectivityOuMark.accessible_by(current_ability).find(params[:id])
    @objectivity_ou_mark.destroy
    respond_to do |format|
      format.html { redirect_to ous_url, notice: t("objectivity_ou_marks.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @objectivity_ou_mark = ObjectivityOuMark.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @objectivity_ou_mark.update objectivity_ou_mark_params
      flash[:success] = t("objectivity_ou_marks.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end


  def objectivity_ou_mark_params
    params.require(:objectivity_ou_mark).permit(:id, :user_id, :monitoring_objectivity_ou_id, :status,
         :mark01, :mark02, :mark03, :mark04, :mark05, :mark06, :mark07, :mark08, :mark09, :mark10,
         :mark11, :mark12, :mark13, :mark14, :mark15, :mark16, :mark17, :mark18, :mark19, :mark20,
         :mark21, :mark22, :mark23, :mark24, :notes)
  end

end
