class OusController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @ous = Ou.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @ous = Ou.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @ous = Ou.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @ous = Ou.accessible_by(current_ability)
    render xlsx: 'ous_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/ous/export.xlsx.axlsx'
  end

  def new
    @ates = Ate.accessible_by(current_ability)
    @mouos = Mouo.accessible_by(current_ability)
    @ou = Ou.new
    if !@ou.nil?
      render :edit
    else
      flash[:success] = t("ous.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @ou = Ou.accessible_by(current_ability).find(params[:id])
  end

  def create
    @ates = Ate.accessible_by(current_ability)
    @mouos = Mouo.accessible_by(current_ability)
    @ou = Ou.new(ou_params)
    if @ou.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ates = Ate.all
    @mouos = Mouo.all
    @ou = Ou.find(params[:id])
  end

  def delete
    Ou.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @ou = Ou.accessible_by(current_ability).find(params[:id])
    @ou.destroy
    respond_to do |format|
      format.html { redirect_to ous_url, notice: t("ous.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @ou = Ou.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @ou.update ou_params
      flash[:success] = t("ous.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end


  def ou_params
    params.require(:ou).permit(:code, :name, :ate_id, :mouo_id, :contact_fio, :contact_phone)
  end

end
