// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

//= require rails-ujs
//= require jquery
//= require best_in_place
//= require jquery_ujs
//= require jquery-ui
//= require best_in_place.jquery-ui
//= require jquery-fileupload
//= require jquery.purr
//= require best_in_place.purr
//= require filterrific/filterrific-jquery


require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("local-time").start()
require("chartkick")
require("chart.js")
require('webpack-jquery-ui');
require('webpack-jquery-ui/css');
require('../vendor/best_in_place.js');
require('../vendor/jquery.purr.js');

window.Rails = Rails

import 'bootstrap'
import 'data-confirm-modal'
import 'bootstrap-icons/font/bootstrap-icons.css'

$(document).on("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
  $(".best_in_place").best_in_place();
});


$(document).ready(function() {
  /* Activating Best In Place */
  jQuery(".best_in_place").best_in_place();
  // jQuery(".best_in_place.purr").best_in_place_purr();
});

require("trix")
require("@rails/actiontext")