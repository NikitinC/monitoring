class ApplicationMailer < ActionMailer::Base
  default from: 'monitor@gia66.ru'
  layout 'mailer'
end
