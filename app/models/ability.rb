# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    if user.admin?
      can :manage, :all
    else
      can :read, :all
    end

    if user.target_type == 2
      user_mouo = Mouo.where(:code => user.target_id).first
      mouo_ids = user_mouo.id
      mouo_code = user_mouo.code
      ate_ids = Ate.where(code: mouo_code).first.id
      ou_ids = Ou.where(ate_id: ate_ids).map(&:id)
      monitoring_objectivity_ou_ids = MonitoringObjectivityOu.joins(:ou).where(" ous.ate_id = #{ate_ids}").map(&:id)
      archive_ou_ids = Archive2021Ou.joins(:ou).where(" ous.ate_id = #{ate_ids}").map(&:id)
      monitoring_objectivity_ate_ids = MonitoringObjectivityAte.joins(:mouo).where(" mouos.id = #{mouo_ids}").map(&:id)
      archive_ate_ids = Archive2021Ate.joins(:mouo).where(" mouos.id = #{mouo_ids}").map(&:id)
    end

    # Это школы
    if user.target_type == 1
      user_ou = Ou.where(:code => user.target_id).first
      ate_ids = Ate.where(:id => user_ou.ate_id).map(&:id)
      mouo_ids = Mouo.where(:id => user_ou.mouo_id).map(&:id)
      ous_from_ate_ids = Ou.where(ate_id: user_ou.ate_id).map(&:id)
      monitoring_objectivity_ou_ids = MonitoringObjectivityOu.joins(:ou).where(" ous.id = #{user_ou.id}").map(&:id)
      archive2021_ids = Archive2021Ou.joins(:ou).where(" ous.id = #{user_ou.id}").map(&:id)
    end

    if user.admin?
      can :manage, Ate
      can :manage, Mouo
      can :manage, Ou
      can :manage, MonitoringObjectivityOu
      can :manage, MonitoringObjectivityAte
      can :manage, ObjectivityAteMark
      can :manage, ObjectivityOuMark
    end
    if user.ous_role? and !user.admin?
      cannot :manage, Ate
      cannot :manage, Mouo
      cannot :manage, Ou
      cannot :manage, MonitoringObjectivityOu
      cannot :manage, MonitoringObjectivityAte
      cannot :manage, Archive2021Ou
      cannot :manage, Archive2021Ate
      can [:read, :export], Archive2021Ou, id: archive2021_ids
      can [:read, :export], Ate, id: ate_ids
      can [:read, :export], Mouo, id: mouo_ids
      can [:read, :export], Ou, id: ous_from_ate_ids
      begin
        can [:read, :export, :create, :update], Ou, id: user_ou.id
      rescue
        puts("")
      end

      can [:read, :export, :create, :update], MonitoringObjectivityOu, id: monitoring_objectivity_ou_ids
      can [:new, :create, :update, :delete, :destroy, :import], MonitoringObjectivityOu
      can :manage, MonitoringObjectivityOusImport

      cannot [:edit, :update, :delete, :destroy], MonitoringObjectivityOu, status_id: 1
      # уже запрещено редактирование
      # cannot :manage, MonitoringObjectivityOu
      # cannot :manage, MonitoringObjectivityAte
      # cannot :manage, MonitoringObjectivityOusImport
      # cannot :manage, MonitoringObjectivityAtesImport
      # can :read, MonitoringObjectivityOusImport
      # can :read, MonitoringObjectivityAtesImport
      # can [:read, :export], MonitoringObjectivityOu, id: monitoring_objectivity_ou_ids
      # can [:read, :export], MonitoringObjectivityAte, id: monitoring_objectivity_ate_ids

    end
    if user.mouo_role? and !user.admin?
      cannot :manage, Ate
      cannot :manage, Mouo
      cannot :manage, Ou
      cannot :manage, MonitoringObjectivityOu
      cannot :manage, MonitoringObjectivityAte
      cannot :manage, Archive2021Ou
      cannot :manage, Archive2021Ate
      can [:read, :export], Archive2021Ou, id: archive_ou_ids
      can [:read, :export], Archive2021Ate, id: archive_ate_ids
      can [:read, :export], Ate, id: ate_ids
      can [:read, :edit, :export], Mouo, id: mouo_ids
      can [:manage], Ou, id: ou_ids
      can [:create], Ou
      # can [:read, :export, :create, :update], Ou, id: user_ou.id
      can [:read, :export, :create, :update, :delete, :destroy, :import], MonitoringObjectivityOu, id: monitoring_objectivity_ou_ids
      can [:new, :create, :import], MonitoringObjectivityOu
      can [:manage], MonitoringObjectivityAte, id: monitoring_objectivity_ate_ids
      can [:new, :create, :import], MonitoringObjectivityAte
      # can :manage, MonitoringObjectivityOusImport
      # can :manage, MonitoringObjectivityAtesImport

      # уже запрещено редактирование
      # can :manage, MonitoringObjectivityOu
      # can :manage, MonitoringObjectivityAte
      # can :manage, MonitoringObjectivityOusImport
      # can :manage, MonitoringObjectivityAtesImport
      # can :read, MonitoringObjectivityOusImport
      # can :read, MonitoringObjectivityAtesImport
      can [:read, :export], MonitoringObjectivityOu, id: monitoring_objectivity_ou_ids
      cannot [:edit, :update, :delete, :destroy], MonitoringObjectivityOu, status_id: 1
      cannot [:edit, :update, :delete, :destroy], MonitoringObjectivityAte, status_id: 1
      # can [:read, :export], MonitoringObjectivityAte, id: monitoring_objectivity_ate_ids

    end

    if user.expert_role?
      can [:read], MonitoringObjectivityAte
      can [:read, :export, :create, :save, :update, :delete, :destroy, :import], ObjectivityAteMark
      can [:read], MonitoringObjectivityOu
      can [:read, :export, :create, :save, :update, :delete, :destroy, :import], ObjectivityOuMark
      if user.id == 1333
        can [:read], :all
        cannot [:import, :create, :new], :all
      end  
    end


    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    can :manage, Announcement
    can :manage, Notification

    # cannot [:export, :import], MonitoringObjectivityOu
    # cannot [:export, :import], MonitoringObjectivityAte

    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end

  # cannot :manage, Ate

end
