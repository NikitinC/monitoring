class Archive2021Ou < ApplicationRecord

  # self.abstract_class = true

  Archive2021Ou.table_name='monitoring_objectivity_ous_2021'

  include PgSearch::Model

  belongs_to :ou
  has_many :archive2021_ou_marks

  def self.search(search)
    if search.length > 0
      self.joins("join ous o ON o.id = monitoring_objectivity_ous_2021.ou_id ").where(" (cast(o.code as varchar(6)) || o.name) LIKE ?", "%#{search}%")
      # where('code || cast(code as varchar(6)) LIKE ?', "%#{search}%")
    else
      self.where("1 = 1")
    end
  end

  def self.filtr(filter)
    if filter.length > 0
      filter = filter.to_i
      self.where(" status_id = #{filter}")
      # where('code || cast(code as varchar(6)) LIKE ?', "%#{filter}%")
    else
      self.where("1 = 1")
    end

  end

  def self.ates(ates)
    if ates.length > 0
      ates = ates.to_i
      self.joins("join ous o ON o.id = monitoring_objectivity_ous_2021.ou_id ").where(" o.ate_id = #{ates}")
      # where('code || cast(code as varchar(6)) LIKE ?', "%#{filter}%")
    else
      self.where("1 = 1")
    end
end


  self.per_page = 20

  WillPaginate.per_page = 20

  has_one_attached :document01
  has_one_attached :document02
  has_one_attached :document03
  has_one_attached :document04
  has_one_attached :document05
  has_one_attached :document06
  has_one_attached :document07
  has_one_attached :document08
  has_one_attached :document09
  has_one_attached :document10
  has_one_attached :document11
  has_one_attached :document12
  has_one_attached :document13
  has_one_attached :document14
  has_one_attached :document15
  has_one_attached :document16

  attr_accessor :delete_document01
  attr_accessor :delete_document02
  attr_accessor :delete_document03
  attr_accessor :delete_document04
  attr_accessor :delete_document05
  attr_accessor :delete_document06
  attr_accessor :delete_document07
  attr_accessor :delete_document08
  attr_accessor :delete_document09
  attr_accessor :delete_document10
  attr_accessor :delete_document11
  attr_accessor :delete_document12
  attr_accessor :delete_document13
  attr_accessor :delete_document14
  attr_accessor :delete_document15
  attr_accessor :delete_document16

  before_validation { document01 = nil if delete_document01 == '1' }

  validates :ou_id, presence: true
  validates :user_id, presence: true
  validates :status_id, presence: true
  validates :url01, url: true
  validates :url02, url: true
  validates :url03, url: true
  validates :url04, url: true
  validates :url05, url: true
  validates :url06, url: true
  validates :url07, url: true
  validates :url08, url: true
  validates :url09, url: true
  validates :url10, url: true
  validates :url11, url: true
  validates :url12, url: true
  validates :url13, url: true
  validates :url14, url: true
  validates :url15, url: true
  validates :url16, url: true

  def entered_number(i)
    if i == 1 and (self.url01.present? or self.document01.present?)
      return true
    end
    if i == 2 and (self.url02.present? or self.document02.present?)
      return true
    end
    if i == 3 and (self.url03.present? or self.document03.present?)
      return true
    end
    if i == 4 and (self.url04.present? or self.document04.present?)
      return true
    end
    if i == 5 and (self.url05.present? or self.document05.present?)
      return true
    end
    if i == 6 and (self.url06.present? or self.document06.present?)
      return true
    end
    if i == 7 and (self.url07.present? or self.document07.present?)
      return true
    end
    if i == 8 and (self.url08.present? or self.document08.present?)
      return true
    end
    if i == 9 and (self.url09.present? or self.document09.present?)
      return true
    end

    if i == 10 and (self.url10.present? or self.document10.present?)
      return true
    end
    if i == 11 and (self.url11.present? or self.document11.present?)
      return true
    end
    if i == 12 and (self.url12.present? or self.document12.present?)
      return true
    end
    if i == 13 and (self.url13.present? or self.document13.present?)
      return true
    end
    if i == 14 and (self.url14.present? or self.document14.present?)
      return true
    end
    if i == 15 and (self.url15.present? or self.document15.present?)
      return true
    end
    if i == 16 and (self.url16.present? or self.document16.present?)
      return true
    end

    return false
  end

  def entered
    count = 0
    i = 0
    while i < 16
      i = i + 1
      if entered_number(i)
        count = count + 1
      end
    end
    return count
  end

end
