class Archive2021OuMark < ApplicationRecord

  # self.abstract_class = true

  Archive2021OuMark.table_name='objectivity_ou_marks_2021'

  belongs_to :archive2021_ou, :foreign_key => :monitoring_objectivity_ou_id, :primary_key => :id
  belongs_to :user

  validates :monitoring_objectivity_ou_id, presence: true
  validates :user_id, presence: true
  validates :mark01, presence: true
  validates :mark02, presence: true
  validates :mark03, presence: true
  validates :mark04, presence: true
  validates :mark05, presence: true
  validates :mark06, presence: true
  validates :mark07, presence: true
  validates :mark08, presence: true
  validates :mark09, presence: true
  validates :mark10, presence: true
  validates :mark11, presence: true
  validates :mark12, presence: true
  validates :mark13, presence: true
  validates :mark14, presence: true
  validates :mark15, presence: true
  validates :mark16, presence: true

  def self.search(search)
    if search.length > 0
      self.joins("join monitoring_objectivity_ous_2021 mon ON mon.id = objectivity_ou_marks_2021.monitoring_objectivity_ou_id join mouos m ON m.id = mon.mouo_id ").where(" (cast(m.code as varchar(6)) || m.name) LIKE ?", "%#{search}%")
      # where('code || cast(code as varchar(6)) LIKE ?', "%#{search}%")
    else
      self.where("1 = 1")
    end
  end


  def self.filtr(filter)
    if filter.length > 0
      filter = filter.to_i
      self.where(" status = #{filter}")
    else
      self.where("1 = 1")
    end
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
