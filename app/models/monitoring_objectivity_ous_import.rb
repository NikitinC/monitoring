class MonitoringObjectivityOusImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    if !attributes.nil?
      attributes.each { |name, value| send("#{name}=", value) }
    else
      return nil
    end
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_monitoring_objectivity_ous
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      monitoring_objectivity_ou = Item.find_by_id(row["id"]) || Item.new
      monitoring_objectivity_ou.attributes = row.to_hash
      monitoring_objectivity_ou
    end
  end

  def imported_monitoring_objectivity_ous
    @imported_monitoring_objectivity_ous ||= load_imported_monitoring_objectivity_ous
  end

  def save
    if imported_monitoring_objectivity_ous.map(&:valid?).all?
      imported_monitoring_objectivity_ous.each(&:save!)
      true
    else
      imported_monitoring_objectivity_ous.each_with_index do |monitoring_objectivity_ou, index|
        monitoring_objectivity_ou.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end