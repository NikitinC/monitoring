class Mouo < ApplicationRecord

  include PgSearch::Model

  belongs_to :ate
  validates :name, presence: true
  validates :code, presence: true
  validates :ate_id, presence: true
  validates :contact_fio, presence: true
  validates :contact_phone, presence: true
  has_many :ous
  has_many :monitoring_objectivity_ates

  def self.search(search)
    if search
      where('name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  def short_name
    ate = Ate.where(:code => self.code).first
    if !ate.nil?
      return "ОМСУ " + ate.name
    else
      if self.code == 95
        return 'ГОУ Свердловской области'
      end
      if self.code == 97
        return 'Негосударственные ОУ'
      end
      return self.name
    end
  end

  def short_name_code
    ate = Ate.where(:code => self.code).first
    if !ate.nil?
      return ("00" + self.code.to_s)[-2..-1] + " ОМСУ " + ate.name
    else
      if self.code == 95
        return '95 ГОУ Свердловской области'
      end
      if self.code == 97
        return '97 Негосударственные ОУ'
      end
      return ("00" + self.code.to_s)[-2..-1] + " " + self.name
    end
  end


  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
