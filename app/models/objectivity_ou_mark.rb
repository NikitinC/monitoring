class ObjectivityOuMark < ApplicationRecord

  include PgSearch::Model

  belongs_to :monitoring_objectivity_ou
  belongs_to :user

  validates :monitoring_objectivity_ou_id, presence: true
  validates :user_id, presence: true
  validates :mark01, presence: true
  validates :mark02, presence: true
  validates :mark03, presence: true
  validates :mark04, presence: true
  validates :mark05, presence: true
  validates :mark06, presence: true
  validates :mark07, presence: true
  validates :mark08, presence: true
  validates :mark09, presence: true
  validates :mark10, presence: true
  validates :mark11, presence: true
  validates :mark12, presence: true
  validates :mark13, presence: true
  validates :mark14, presence: true
  validates :mark15, presence: true
  validates :mark16, presence: true
  validates :mark17, presence: true
  validates :mark18, presence: true
  validates :mark19, presence: true
  validates :mark20, presence: true
  validates :mark21, presence: true
  validates :mark22, presence: true
  validates :mark23, presence: true
  validates :mark24, presence: true

  has_rich_text :notes

  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.notes.to_s).html_safe
    # rescue innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    innertext
  end

  def self.search(search)
    if search.length > 0
      self.joins("join monitoring_objectivity_ous mon ON mon.id = objectivity_ou_marks.monitoring_objectivity_ou_id join mouos m ON m.id = mon.mouo_id ").where(" (cast(m.code as varchar(6)) || m.name) LIKE ?", "%#{search}%")
      # where('code || cast(code as varchar(6)) LIKE ?', "%#{search}%")
    else
      self.where("1 = 1")
    end
  end

  def marksum
    begin
      return (self.mark01 + self.mark02 + self.mark03 + self.mark04 + self.mark05 + self.mark06 + self.mark07 + self.mark08 + self.mark09 + self.mark10 +
        self.mark11 + self.mark12 + self.mark13 + self.mark14 + self.mark15 + self.mark16 + self.mark17 + self.mark18 + self.mark19 + self.mark20 +
        self.mark21 + self.mark22 + self.mark23 + self.mark24)
    rescue
      return("Не заполнено")
    end
  end


  def self.filtr(filter)
    if filter.length > 0
      filter = filter.to_i
      self.where(" status = #{filter}")
    else
      self.where("1 = 1")
    end
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
