class Ou < ApplicationRecord
  belongs_to :mouo
  belongs_to :ate

  has_many :monitoring_objectivity_ou

  def self.search(search)
    if search
      where('name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

  def to_s
    "#{code} ..#{name[-50..-1]}"
  end

end
