require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)


module Monitoring
  class Application < Rails::Application
    Sprockets.export_concurrent = false
    config.active_job.queue_adapter = :sidekiq
    config.application_name = Rails.application.class.module_parent_name
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.i18n.load_path += Dir[Rails.root.join("config", "locales", "**", "*.{rb,yml}").to_s]
    config.i18n.available_locales = [:ru, :en]
    config.i18n.default_locale = :ru
    config.i18n.fallbacks = true
    # config.i18n.fallbacks = [:en]
    #
    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Asia (Yekaterinburg)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
