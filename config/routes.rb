require 'sidekiq/web'
# root 'pages#home'
# skip_before_action :authenticate_user!, :only => [:index]

Rails.application.routes.draw do
  # devise_for :users

  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  namespace :admin do
    resources :users
    resources :announcements
    resources :notifications
    resources :services
    root to: "users#index"
  end

  get '/ates/export', to: 'ates#export'
  resources :ates
  resources :ates_imports, only: [:new, :create]

  get '/objectivity_ate_marks/export', to: 'objectivity_ate_marks#export'
  resources :objectivity_ate_marks
  resources :objectivity_ate_marks_imports, only: [:new, :create]

  get '/objectivity_ou_marks/export', to: 'objectivity_ou_marks#export'
  resources :objectivity_ou_marks
  resources :objectivity_ou_marks_imports, only: [:new, :create]


  get '/monitoring_objectivity_ous/export', to: 'monitoring_objectivity_ous#export'
  resources :monitoring_objectivity_ous
  resources :monitoring_objectivity_ous_imports, only: [:new, :create]

  get '/monitoring_objectivity_ates/export', to: 'monitoring_objectivity_ates#export'
  resources :monitoring_objectivity_ates
  resources :monitoring_objectivity_ates_imports, only: [:new, :create]

  get '/mouos/export', to: 'mouos#export'
  resources :mouos
  resources :mouos_imports, only: [:new, :create]

  get '/ous/export', to: 'ous#export'
  resources :ous
  resources :ous_imports, only: [:new, :create]

  get '/archive2021_ous/export', to: 'archive2021_ous#export'
  resources :archive2021_ous

  get '/archive2021_ates/export', to: 'archive2021_ates#export'
  resources :archive2021_ates

  get '/archive2021_ou_marks/export', to: 'archive2021_ou_marks#export'
  resources :archive2021_ou_marks

  get '/archive2021_ate_marks/export', to: 'archive2021_ate_marks#export'
  resources :archive2021_ate_marks

  get '/recreate_logins_for_all_users', to: 'home#recreate_logins_for_all_users'
  get '/help', to: 'home#help'
  get '/privacy', to: 'home#privacy'
  get '/terms', to: 'home#terms'
  get '/monitoring_objectivity_ates_imports', to: 'monitoring_objectivity_ates_imports#new'
  get '/monitoring_objectivity_ous_imports', to: 'monitoring_objectivity_ous_imports#new'

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  resources :notifications, only: [:index]
  resources :announcements, only: [:index]

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  # devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}
  # root :to => redirect("/users/sign_in")

  root :to => 'home#index'

  #
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
