class AddRolesToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :target_type, :integer
    add_column :users, :target_id, :integer
    add_column :users, :superadmin_role, :boolean, default: false
    add_column :users, :mouo_role, :boolean, default: false
    add_column :users, :ous_role, :boolean, default: true
    add_column :users, :expert_role, :boolean, default: true
  end
end
