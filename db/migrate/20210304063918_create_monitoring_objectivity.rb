class CreateMonitoringObjectivity < ActiveRecord::Migration[6.1]
  def change
    create_table :monitoring do |t|
      t.integer :target_type
      t.integer :target_id
      t.integer :user_id
      t.integer :status_id

      t.string  :url01
      t.string  :document01
      t.string  :url02
      t.string  :document02
      t.string  :url03
      t.string  :document03
      t.string  :url04
      t.string  :document04
      t.string  :url05
      t.string  :document05
      t.string  :url06
      t.string  :document06
      t.string  :url07
      t.string  :document07
      t.string  :url08
      t.string  :document08
      t.string  :url09
      t.string  :document09
      t.string  :url10
      t.string  :document10
      t.string  :url11
      t.string  :document11
      t.string  :url12
      t.string  :document12
      t.string  :url13
      t.string  :document13
      t.string  :url14
      t.string  :document14
      t.string  :url15
      t.string  :document15
      t.string  :url16
      t.string  :document16
      t.string  :url17
      t.string  :document17
      t.string  :url18
      t.string  :document18
      t.string  :url19
      t.string  :document19
      t.string  :url20
      t.string  :document20

      t.timestamps
    end
  end
end
