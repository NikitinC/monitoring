class DropTableOus < ActiveRecord::Migration[6.1]
  def change
    drop_table :ous
    create_table :ous do |t|
      t.belongs_to :ate, foreign_key: true
      t.belongs_to :mouo, foreign_key: true
      t.integer :code
      t.string  :name
      t.string  :contact_fio
      t.string  :contact_phone
      t.timestamps
    end

  end
end
