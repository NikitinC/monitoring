class CreateMonitoringObjectivityAtes < ActiveRecord::Migration[6.1]
  def change
    create_table :monitoring_objectivity_ates do |t|
      t.integer :mouo_id
      t.integer :user_id
      t.integer :status_id

      t.string  :url01
      t.string  :url02
      t.string  :url03
      t.string  :url04
      t.string  :url05
      t.string  :url06
      t.string  :url07
      t.string  :url08
      t.string  :url09
      t.string  :url10
      t.string  :url11

      t.timestamps
    end
  end
end
