class CreateObjectivityAteMarks < ActiveRecord::Migration[6.1]
  def change
    create_table :objectivity_ate_marks do |t|
      t.belongs_to :monitoring_objectivity_ate, column: :id
      t.belongs_to :user, column: :id
      t.integer :status
      t.integer :mark01
      t.integer :mark02
      t.integer :mark03
      t.integer :mark04
      t.integer :mark05
      t.integer :mark06
      t.integer :mark07
      t.integer :mark08
      t.integer :mark09
      t.integer :mark10
      t.integer :mark11
      t.string :notes
      t.timestamps
    end
  end
end
