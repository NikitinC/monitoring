class AddFieldsToMonitoringObjectivityAtes < ActiveRecord::Migration[6.1]
  def change
    add_column :monitoring_objectivity_ates, :url12, :string
    add_column :monitoring_objectivity_ates, :url13, :string
    add_column :monitoring_objectivity_ates, :url14, :string
    add_column :monitoring_objectivity_ates, :url15, :string
    add_column :monitoring_objectivity_ates, :url16, :string
    add_column :monitoring_objectivity_ates, :url17, :string

    add_column :monitoring_objectivity_ous, :url17, :string
    add_column :monitoring_objectivity_ous, :url18, :string
    add_column :monitoring_objectivity_ous, :url19, :string
    add_column :monitoring_objectivity_ous, :url20, :string
    add_column :monitoring_objectivity_ous, :url21, :string
    add_column :monitoring_objectivity_ous, :url22, :string

  end
end
