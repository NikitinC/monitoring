class AddFieldsToMarks < ActiveRecord::Migration[6.1]
  def change
    add_column :objectivity_ate_marks, :mark12, :integer
    add_column :objectivity_ate_marks, :mark13, :integer
    add_column :objectivity_ate_marks, :mark14, :integer
    add_column :objectivity_ate_marks, :mark15, :integer
    add_column :objectivity_ate_marks, :mark16, :integer
    add_column :objectivity_ate_marks, :mark17, :integer
    add_column :objectivity_ate_marks, :mark18, :integer
    add_column :objectivity_ate_marks, :mark19, :integer
    add_column :objectivity_ate_marks, :mark20, :integer
    add_column :objectivity_ate_marks, :mark21, :integer
    add_column :objectivity_ate_marks, :mark22, :integer
    add_column :objectivity_ate_marks, :mark23, :integer
    add_column :objectivity_ate_marks, :mark24, :integer
    add_column :objectivity_ate_marks, :mark25, :integer
    add_column :objectivity_ate_marks, :mark26, :integer
    add_column :objectivity_ate_marks, :mark27, :integer
    add_column :objectivity_ate_marks, :mark28, :integer
    add_column :objectivity_ate_marks, :mark29, :integer
    add_column :objectivity_ate_marks, :mark30, :integer
    add_column :objectivity_ate_marks, :mark31, :integer
    add_column :objectivity_ate_marks, :mark32, :integer
    add_column :objectivity_ate_marks, :mark33, :integer
    add_column :objectivity_ate_marks, :mark34, :integer
    add_column :objectivity_ate_marks, :mark35, :integer
    add_column :objectivity_ate_marks, :mark36, :integer
    add_column :objectivity_ate_marks, :mark37, :integer
    add_column :objectivity_ate_marks, :mark38, :integer
    add_column :objectivity_ate_marks, :mark39, :integer
    add_column :objectivity_ate_marks, :mark40, :integer
    add_column :objectivity_ate_marks, :mark41, :integer


    add_column :objectivity_ou_marks, :mark17, :integer
    add_column :objectivity_ou_marks, :mark18, :integer
    add_column :objectivity_ou_marks, :mark19, :integer
    add_column :objectivity_ou_marks, :mark20, :integer
    add_column :objectivity_ou_marks, :mark21, :integer
    add_column :objectivity_ou_marks, :mark22, :integer
    add_column :objectivity_ou_marks, :mark23, :integer
    add_column :objectivity_ou_marks, :mark24, :integer
  end
end
