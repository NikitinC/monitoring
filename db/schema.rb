# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_24_101641) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "announcements", force: :cascade do |t|
    t.datetime "published_at"
    t.string "announcement_type"
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ates", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "monitoring_objectivity_ates", force: :cascade do |t|
    t.integer "mouo_id"
    t.integer "user_id"
    t.integer "status_id"
    t.string "url01"
    t.string "url02"
    t.string "url03"
    t.string "url04"
    t.string "url05"
    t.string "url06"
    t.string "url07"
    t.string "url08"
    t.string "url09"
    t.string "url10"
    t.string "url11"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "url12"
    t.string "url13"
    t.string "url14"
    t.string "url15"
    t.string "url16"
    t.string "url17"
  end

  create_table "monitoring_objectivity_ates_2021", id: :bigint, default: -> { "nextval('monitoring_objectivity_ates_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "mouo_id"
    t.integer "user_id"
    t.integer "status_id"
    t.string "url01"
    t.string "url02"
    t.string "url03"
    t.string "url04"
    t.string "url05"
    t.string "url06"
    t.string "url07"
    t.string "url08"
    t.string "url09"
    t.string "url10"
    t.string "url11"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "monitoring_objectivity_ous", force: :cascade do |t|
    t.integer "user_id"
    t.integer "status_id"
    t.string "url01"
    t.string "document01"
    t.string "url02"
    t.string "url03"
    t.string "url04"
    t.string "url05"
    t.string "url06"
    t.string "url07"
    t.string "url08"
    t.string "url09"
    t.string "url10"
    t.string "url11"
    t.string "url12"
    t.string "url13"
    t.string "url14"
    t.string "url15"
    t.string "url16"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "ou_id"
    t.string "url17"
    t.string "url18"
    t.string "url19"
    t.string "url20"
    t.string "url21"
    t.string "url22"
    t.index ["ou_id"], name: "index_monitoring_objectivity_ous_on_ou_id"
  end

  create_table "monitoring_objectivity_ous_2021", id: :bigint, default: -> { "nextval('monitoring_objectivity_ous_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "user_id"
    t.integer "status_id"
    t.string "url01"
    t.string "document01"
    t.string "url02"
    t.string "url03"
    t.string "url04"
    t.string "url05"
    t.string "url06"
    t.string "url07"
    t.string "url08"
    t.string "url09"
    t.string "url10"
    t.string "url11"
    t.string "url12"
    t.string "url13"
    t.string "url14"
    t.string "url15"
    t.string "url16"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "ou_id"
  end

  create_table "mouos", force: :cascade do |t|
    t.bigint "ate_id"
    t.integer "code"
    t.string "name"
    t.string "contact_fio"
    t.string "contact_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ate_id"], name: "index_mouos_on_ate_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "recipient_type", null: false
    t.bigint "recipient_id", null: false
    t.string "type", null: false
    t.jsonb "params"
    t.datetime "read_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["read_at"], name: "index_notifications_on_read_at"
    t.index ["recipient_type", "recipient_id"], name: "index_notifications_on_recipient"
  end

  create_table "objectivity_ate_marks", force: :cascade do |t|
    t.bigint "monitoring_objectivity_ate_id"
    t.bigint "user_id"
    t.integer "status"
    t.integer "mark01"
    t.integer "mark02"
    t.integer "mark03"
    t.integer "mark04"
    t.integer "mark05"
    t.integer "mark06"
    t.integer "mark07"
    t.integer "mark08"
    t.integer "mark09"
    t.integer "mark10"
    t.integer "mark11"
    t.string "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "mark12"
    t.integer "mark13"
    t.integer "mark14"
    t.integer "mark15"
    t.integer "mark16"
    t.integer "mark17"
    t.integer "mark18"
    t.integer "mark19"
    t.integer "mark20"
    t.integer "mark21"
    t.integer "mark22"
    t.integer "mark23"
    t.integer "mark24"
    t.integer "mark25"
    t.integer "mark26"
    t.integer "mark27"
    t.integer "mark28"
    t.integer "mark29"
    t.integer "mark30"
    t.integer "mark31"
    t.integer "mark32"
    t.integer "mark33"
    t.integer "mark34"
    t.integer "mark35"
    t.integer "mark36"
    t.integer "mark37"
    t.integer "mark38"
    t.integer "mark39"
    t.integer "mark40"
    t.integer "mark41"
    t.index ["monitoring_objectivity_ate_id"], name: "index_objectivity_ate_marks_on_monitoring_objectivity_ate_id"
    t.index ["user_id"], name: "index_objectivity_ate_marks_on_user_id"
  end

  create_table "objectivity_ate_marks_2021", id: :bigint, default: -> { "nextval('objectivity_ate_marks_id_seq'::regclass)" }, force: :cascade do |t|
    t.bigint "monitoring_objectivity_ate_id"
    t.bigint "user_id"
    t.integer "status"
    t.integer "mark01"
    t.integer "mark02"
    t.integer "mark03"
    t.integer "mark04"
    t.integer "mark05"
    t.integer "mark06"
    t.integer "mark07"
    t.integer "mark08"
    t.integer "mark09"
    t.integer "mark10"
    t.integer "mark11"
    t.string "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "objectivity_ou_marks", force: :cascade do |t|
    t.bigint "monitoring_objectivity_ou_id"
    t.bigint "user_id"
    t.integer "status"
    t.integer "mark01"
    t.integer "mark02"
    t.integer "mark03"
    t.integer "mark04"
    t.integer "mark05"
    t.integer "mark06"
    t.integer "mark07"
    t.integer "mark08"
    t.integer "mark09"
    t.integer "mark10"
    t.integer "mark11"
    t.integer "mark12"
    t.integer "mark13"
    t.integer "mark14"
    t.integer "mark15"
    t.integer "mark16"
    t.string "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "mark17"
    t.integer "mark18"
    t.integer "mark19"
    t.integer "mark20"
    t.integer "mark21"
    t.integer "mark22"
    t.integer "mark23"
    t.integer "mark24"
    t.index ["monitoring_objectivity_ou_id"], name: "index_objectivity_ou_marks_on_monitoring_objectivity_ou_id"
    t.index ["user_id"], name: "index_objectivity_ou_marks_on_user_id"
  end

  create_table "objectivity_ou_marks_2021", id: :bigint, default: -> { "nextval('objectivity_ou_marks_id_seq'::regclass)" }, force: :cascade do |t|
    t.bigint "monitoring_objectivity_ou_id"
    t.bigint "user_id"
    t.integer "status"
    t.integer "mark01"
    t.integer "mark02"
    t.integer "mark03"
    t.integer "mark04"
    t.integer "mark05"
    t.integer "mark06"
    t.integer "mark07"
    t.integer "mark08"
    t.integer "mark09"
    t.integer "mark10"
    t.integer "mark11"
    t.integer "mark12"
    t.integer "mark13"
    t.integer "mark14"
    t.integer "mark15"
    t.integer "mark16"
    t.string "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ous", force: :cascade do |t|
    t.bigint "ate_id"
    t.bigint "mouo_id"
    t.integer "code"
    t.string "name"
    t.string "contact_fio"
    t.string "contact_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ate_id"], name: "index_ous_on_ate_id"
    t.index ["mouo_id"], name: "index_ous_on_mouo_id"
  end

  create_table "services", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "provider"
    t.string "uid"
    t.string "access_token"
    t.string "access_token_secret"
    t.string "refresh_token"
    t.datetime "expires_at"
    t.text "auth"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_services_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.datetime "announcements_last_read_at"
    t.boolean "admin", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "target_type"
    t.integer "target_id"
    t.boolean "superadmin_role", default: false
    t.boolean "mouo_role", default: false
    t.boolean "ous_role", default: true
    t.boolean "expert_role", default: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "monitoring_objectivity_ous", "ous"
  add_foreign_key "monitoring_objectivity_ous_2021", "ous", name: "fk_rails_727699f801_2021"
  add_foreign_key "mouos", "ates"
  add_foreign_key "ous", "ates"
  add_foreign_key "ous", "mouos"
  add_foreign_key "services", "users"
end
